// Copyright 2017 Bastian Blank <waldi@debian.org>
// See LICENSE file.

package main

import (
	"time"
)

type LogMsg struct {
	Timestamp time.Time
	RequestMethod string
	Urlid string
	Status string
	ResponseSize string
	Cache string
	State string
}
