// Copyright 2017 Bastian Blank <waldi@debian.org>
// See LICENSE file.

package main

import (
	"context"
	"regexp"
	"strings"
	"time"

	"gopkg.in/mcuadros/go-syslog.v2"
)

var stateHit = regexp.MustCompile(`^HIT(-|$)`)
var stateMiss = regexp.MustCompile(`^MISS(-|$)`)
var statePass = regexp.MustCompile(`^PASS(-|$)`)
var stateError = regexp.MustCompile(`^(BG-)?ERROR(-|$)`)

func receiveLog(ctx context.Context, sinks ...sink) (err error) {
	channel := make(syslog.LogPartsChannel)
	handler := syslog.NewChannelHandler(channel)

	server := syslog.NewServer()
	server.SetFormat(syslog.RFC5424)
	server.SetHandler(handler)
	server.ListenTCP(listenSyslog)
	server.Boot()

	go func() {
		for i := range channel {
			t := i["timestamp"]
			m := i["message"]
			if t == nil || m == nil {
				continue
			}

			d := strings.Split(m.(string), " ")
			if len(d) < 4 {
				continue
			}

			state := d[4]
			cache := "unknown"
			switch {
			case stateHit.MatchString(state):
				cache = "hit"
			case stateMiss.MatchString(state):
				cache = "miss"
			case statePass.MatchString(state):
				cache = "pass"
			case stateError.MatchString(state):
				cache = "error"
			}

			msgdata := LogMsg{
				Timestamp: t.(time.Time),
				RequestMethod: d[0],
				Urlid: d[1],
				Status: d[2],
				ResponseSize: d[3],
				State: state,
				Cache: cache,
			}

			for _, s := range sinks {
				s.Message(ctx, &msgdata)
			}
		}
	}()

	server.Wait()

	return
}
