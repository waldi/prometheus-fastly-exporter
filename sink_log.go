// Copyright 2017 Bastian Blank <waldi@debian.org>
// See LICENSE file.

package main

import (
	"context"
	"fmt"
	"io"
	"os"
	"time"
)

type sinkLog struct {
	log io.WriteCloser
}

func newSinkLog() sink {
	return sinkLog{
		log: os.Stdout,
	}
}

func newSinkLogFile(path string, sig os.Signal) (sink, error) {
	log, err := NewLogfile(path, sig)
	if (err != nil) {
		return nil, err
	}
	return sinkLog{
		log: log,
	}, nil
}

func (self sinkLog) Message(ctx context.Context, msg *LogMsg) {
	fmt.Fprintf(self.log.(io.Writer), "0.0.0.0 - - %s \"%s '%s' HTTP/1.1\" %s %s - \"-\" cache=%s fastly_state=%s\n",
		msg.Timestamp.Format(time.RFC3339),
		msg.RequestMethod,
		msg.Urlid,
		msg.Status,
		msg.ResponseSize,
		msg.Cache,
		msg.State,
	)
}
