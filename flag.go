// Copyright 2017 Bastian Blank <waldi@debian.org>
// See LICENSE file.

package main

import (
	"errors"
	"strings"
)

type mapValue map[string]string

func (i mapValue) Set(value string) error {
	s := strings.SplitN(value, "=", 2)
	if len(s) < 2 {
		return errors.New("")
	}
	i[s[0]] = s[1]
	return nil
}

func (i mapValue) String() string {
	return ""
}

func (i mapValue) Type() string {
	return "key=value"
}
