// Copyright 2017 Bastian Blank <waldi@debian.org>
// See LICENSE file.

package main

import (
	"context"
	"log"
	"syscall"

	"github.com/spf13/cobra"
)

var (
	logFile string
	logStdout bool
	listen string
	listenSyslog string
)

func run(cmd *cobra.Command, args []string) {
	ctx := context.Background()

	var sinks []sink
	sinks = append(sinks, newSinkPrometheus(listen, syscall.SIGUSR1))
	if logFile != "" {
		sink, err := newSinkLogFile(logFile, syscall.SIGUSR1)
		if err != nil {
			log.Fatal("Failed to open log file: ", err)
		}
		sinks = append(sinks, sink)
	}
	if logStdout {
		sinks = append(sinks, newSinkLog())
	}

	err := receiveLog(ctx, sinks...)
	log.Fatal(err)
}

var cmd = &cobra.Command{
	Use: "prometheus-fastly-exporter",
	Run: run,
}

func init() {
	cmd.Flags().StringVar(&listen, "listen", "127.0.0.1:9122", "Address to listen on")
	cmd.Flags().StringVar(&listenSyslog, "listen-syslog", "0.0.0.0:2514", "Address to listen on for syslog traffic")
	cmd.Flags().StringVar(&logFile, "log-file", "", "Write a web server log to file (use SIGUSR1 to re-open)")
	cmd.Flags().BoolVar(&logStdout, "log-stdout", false, "Write a web server log to stdout")
}

func main() {
	cmd.Execute()
}
